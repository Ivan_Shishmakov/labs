#include <gtest/gtest.h>
#include "trit.h"
#include "trit_set.h"

using namespace TernarySpace;

void FillTritSet(TritSet& set)
{
	for (size_t i = 0; i < set.Size(); ++i)
	{
		if (i % 2 == 0)
			set[i] = Trit::True;
		else
			set[i] = Trit::False;
	}
}

TEST(TritSetConstructorTest, DefaultTest)
{
	TritSet set(200);
	for (size_t i = 0; i < 200; ++i)
		set[i] = Trit::False;

	ASSERT_EQ(set.Size(), 200);
	ASSERT_LE(set.Capacity(), 200);

	for (size_t i = 0; i < 200; ++i)
		ASSERT_EQ((Trit)set[i], Trit::False);
}

TEST(TritSetConstructorTest, NoMemory)
{
	TritSet set;

	ASSERT_EQ(set.Size(), 0);
	ASSERT_EQ(set.Capacity(), 0);
}

TEST(TritSetConstructorTest, AllocatingMemory)
{
	TritSet set(100);

	ASSERT_EQ(set.Size(), 100);
	ASSERT_LE(set.Capacity(), 100);
}

TEST(OperatorsTritSetTest, OperatorGet)
{
	TritSet set;
	set[0] = Trit::False;
	set[5] = Trit::True;

	Trit a = set[5];

	ASSERT_EQ((Trit)set[0], Trit::False);
	ASSERT_EQ((Trit)set[5], Trit::True);
	ASSERT_EQ((Trit)a, Trit::True);
	ASSERT_EQ((Trit)set[10000], Trit::Unknown);
	ASSERT_EQ(set.Size(), 6);
}

TEST(OperatorsTritSetTest, ComparisonOperator)
{
	TritSet set;
	set[0] = Trit::False;
	set[5] = Trit::True;

	Trit a = set[5];

	ASSERT_EQ(set[0] == Trit::False, 1);
	ASSERT_EQ(set[5] == Trit::True, 1);
	ASSERT_EQ(a == Trit::True, 1);
	ASSERT_EQ(a == set[0], 0);
	ASSERT_EQ(set[10000] == Trit::Unknown, 1);
}

TEST(TritSetMethodsTest, CardinalityMethod)
{
	TritSet set(100);
	FillTritSet(set);
	

	ASSERT_EQ(set.Cardinality(Trit::True), 50);
	ASSERT_EQ(set.Cardinality(Trit::False), 50);
	ASSERT_EQ(set.Cardinality(Trit::Unknown), 0);
	ASSERT_EQ(set.Size(), 100);

	set[10000] = Trit::Unknown;
	ASSERT_EQ(set.Size(), 100);

	set[10000] = Trit::True;
	ASSERT_EQ(set.Size(), 10001);
	ASSERT_EQ(set.Cardinality(Trit::True), 51);
	ASSERT_NE(set.Cardinality(Trit::Unknown), 0);

	set[10000] = Trit::Unknown;
	ASSERT_EQ(set.Cardinality(Trit::True), 50);
	ASSERT_EQ(set.Cardinality(Trit::False), 50);
	ASSERT_EQ(set.Size(), 100);
	ASSERT_EQ(set.Cardinality(Trit::Unknown), 0);
}

TEST(TritSetMethodsTest, LengthMethodTest)
{
	TritSet set(100);
	ASSERT_EQ(set.Length(), 0);

	set[3] = Trit::False;
	ASSERT_EQ(set.Length(), 4);

	set[3] = Trit::Unknown;
	ASSERT_NE(set.Length(), 4);
	ASSERT_EQ(set.Length(), 0);
}

TEST(TritSetMethodsTest, TrimMethodTest)
{
	TritSet set(200);
	FillTritSet(set);
	ASSERT_EQ(set.Cardinality(Trit::True), 100);
	ASSERT_EQ(set.Cardinality(Trit::False), 100);

	set.Trim(100);
	for (size_t i = 100; i < 200; ++i)
		ASSERT_EQ((Trit)set[i], Trit::Unknown);

	ASSERT_NE(set.Cardinality(Trit::True), 100);
	ASSERT_NE(set.Cardinality(Trit::False), 100);
}

TEST(TritSetMethodsTest, ShrinkMethodTest)
{
	TritSet set(50);
	ASSERT_EQ(set.Capacity(), 2);
	set[2] = Trit::True;
	set.Shrink();

	ASSERT_NE(set.Capacity(), 2);
	ASSERT_NE(set.Size(), 50);
}

TEST(TritSetMethodsTest, AssignmentOperatorTest)
{
	TritSet setA(100);
	TritSet setB(200);

	FillTritSet(setA);
	FillTritSet(setB);

	TritSet setC;
	setC = setB;

	ASSERT_EQ(setC == setB, true);

	setC[2] = Trit::Unknown;
	ASSERT_EQ(setC != setB, true);
}

TEST(TritSetMethodsTest, CopyConstructorTest)
{
	TritSet setA(200);
	FillTritSet(setA);
	TritSet setB = setA;

	ASSERT_EQ(setB == setA, true);
}

TEST(TritSetMethodsTest, BitWiseAndTest)
{
	TritSet setA(200);
	TritSet setB(300);

	FillTritSet(setA);

	TritSet setC = setB & setA;
	ASSERT_EQ(setC.Capacity(), setB.Capacity());

	for (size_t i = 0; i < setC.Size(); ++i)
	{
		if (i % 2 == 0)
			ASSERT_EQ((Trit)setC[i], Trit::Unknown);
		else
			ASSERT_EQ((Trit)setC[i], Trit::False);
	}

	ASSERT_EQ(setC.Cardinality(Trit::Unknown), setC.Cardinality(Trit::False));
}

TEST(TritSetMethodsTest, BitWiseOrTest)
{
	TritSet setA(200);
	TritSet setB(300);

	FillTritSet(setA);

	TritSet setC = setB | setA;

	ASSERT_EQ(setC.Capacity(), setB.Capacity());

	for (size_t i = 0; i < setC.Size(); ++i)
	{
		if (i % 2 == 0)
			ASSERT_EQ((Trit)setC[i], Trit::True);
		else
			ASSERT_EQ((Trit)setC[i], Trit::Unknown);
	}
}

TEST(TritSetMethodsTest, BitWiseNotTest)
{
	TritSet setA(200);
	FillTritSet(setA);

	TritSet setC = ~setA;

	ASSERT_EQ(setC.Capacity(), setA.Capacity());

	for (size_t i = 0; i < setC.Size(); ++i)
		ASSERT_NE((Trit)setC[i], (Trit)setA[i]);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
