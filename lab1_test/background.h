#ifndef BACKGROUND_H
#define BACKGROUND_H
#include "trit.h"
#include <cstddef>

namespace Background
{
	size_t GetArrayIndex(const size_t);
	int GetBitIndex(const size_t);
	bool NotInRange(size_t, const size_t);
	void SetValues(int&, int&, TernarySpace::Trit);
	void SetBit(size_t&, int, int);
}

#endif
