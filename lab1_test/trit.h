#ifndef TRIT_H
#define TRIT_H

namespace TernarySpace
{
	enum class Trit { False = 0b01, Unknown = 0b00, True = 0b10 };

	Trit operator ~ (Trit);
	Trit operator & (Trit, Trit);
	Trit operator | (Trit, Trit);
}

#endif