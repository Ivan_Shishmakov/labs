#ifndef CONSTANTS_H
#define CONSTANTS_H
#include <climits>
#include <cstddef>

namespace CONSTANTS
{
	const size_t SIZE_T_IN_BITS = sizeof(size_t) * CHAR_BIT;
	const size_t SHIFT_FOR_ALLOCATION = 63;
	const size_t SHIFT = SIZE_T_IN_BITS;
	const size_t TRIT_SIZE = 2;
	const size_t TRITS_IN_SIZE_T = 32;
	const size_t FIRST_MASK = 0xFFFFFFFFFFFFFFFF;
	const size_t SECOND_MASK = 0b11;
	const size_t UPPER = 1;
	const size_t LOWERER = 1;
	const size_t BIT_SHIFTER = 2;
	const size_t ERROR = -1;
	const size_t DEFAULT_VALUE = 0;
}

#endif
