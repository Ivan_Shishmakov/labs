#include "trit_set.h"
#include "constants.h"
#include "background.h"

namespace TernarySpace
{
	TritSet::TritSet(size_t logical_size)
	{
		_array_size = (logical_size * CONSTANTS::TRIT_SIZE + CONSTANTS::SHIFT_FOR_ALLOCATION) / CONSTANTS::SIZE_T_IN_BITS;
		_logical_size = logical_size;
		_trits.resize(_array_size);
	}

	TritSet::~TritSet() {}

	TritSet::TritSet(const TritSet& set)
	{
		_array_size = set._array_size;
		_logical_size = set._logical_size;
		_true_trit_num = set._true_trit_num;
		_false_trit_num = set._false_trit_num;
		_unknown_trit_num = set._unknown_trit_num;

		_trits.resize(_array_size);

		for (size_t i = 0; i < _array_size; ++i)
			_trits[i] = set.GetArrayValue(i);
	}

	TritSet& TritSet::operator = (const TritSet& set)
	{
		if (this == &set)
			return *this;

		_trits.clear();
		_array_size = set._array_size;
		_logical_size = set._logical_size;
		_true_trit_num = set._true_trit_num;
		_false_trit_num = set._false_trit_num;
		_unknown_trit_num = set._unknown_trit_num;

		_trits.resize(_array_size);

		for (size_t i = 0; i < _array_size; ++i)
			_trits[i] = set.GetArrayValue(i);
        return *this;
	}

	TritSet::Reference::Reference(TritSet& trit_set, size_t trit_number) : _trit_set(trit_set), _trit_number(trit_number) {}

	TritSet::Reference::~Reference() {}

	TritSet::Reference TritSet::operator [] (size_t trit_number)
	{
		return Reference(*this, trit_number);
	}

	Trit TritSet::operator [] (size_t trit_number) const
	{
		return GetTrit(trit_number);
	}

	Trit TritSet::GetTrit(size_t trit_number) const
	{
		size_t array_index = Background::GetArrayIndex(trit_number);
		if (Background::NotInRange(array_index, _array_size))
			return Trit::Unknown;

		int bit_number = Background::GetBitIndex(trit_number);

		return static_cast<Trit>((_trits[array_index] >> bit_number) & CONSTANTS::SECOND_MASK);
	}

	void TritSet::ChangeArraySize(size_t array_index)
	{
		_array_size = ++array_index;
		_trits.resize(_array_size);
	}

	void TritSet::ChangeTritsNumber(const Trit& new_trit, const Trit& cur_trit)
	{
		if (cur_trit == Trit::True)
			--_true_trit_num;
		else if (cur_trit == Trit::False)
			--_false_trit_num;
		else if (_unknown_trit_num)
			--_unknown_trit_num;

		if (new_trit == Trit::True)
			++_true_trit_num;
		else if (new_trit == Trit::False)
			++_false_trit_num;
		else
			++_unknown_trit_num;
	}

	void TritSet::ChangeTritsNumber()
	{
		_logical_size = Length();
		_unknown_trit_num = _false_trit_num = _true_trit_num = CONSTANTS::DEFAULT_VALUE;
		
		if (_logical_size)
		{
			for (size_t i = 0; i < _logical_size; ++i)
			{
				Trit res = GetTrit(i);
				if (res == Trit::True)
					++_true_trit_num;
				else if (res == Trit::False)
					++_false_trit_num;
				else
					++_unknown_trit_num;
			}
		}
	}

	size_t TritSet::Length() const
	{
		size_t idx = _array_size * CONSTANTS::TRITS_IN_SIZE_T - CONSTANTS::LOWERER;

		for (long long i = idx; i >= 0; --i)
		{
			if (GetTrit(i) != Trit::Unknown)
				return i + CONSTANTS::UPPER;
		}

		return CONSTANTS::DEFAULT_VALUE;
	}

	void TritSet::SetTrit(Trit new_trit, size_t trit_number)
	{
		size_t array_index = Background::GetArrayIndex(trit_number);

		if (Background::NotInRange(array_index, _array_size))
		{
			if (new_trit == Trit::Unknown)
				return;
			ChangeArraySize(array_index);
		}

		int bit_number = Background::GetBitIndex(trit_number);
		Trit cur_trit = GetTrit(trit_number);

		ChangeTritsNumber(new_trit, cur_trit);

		int first_bit_value = 0, second_bit_value = 0;
		Background::SetValues(first_bit_value, second_bit_value, new_trit);
		Background::SetBit(_trits[array_index], bit_number, first_bit_value);
		Background::SetBit(_trits[array_index], bit_number + CONSTANTS::UPPER, second_bit_value);

		bool is_changed = false;

		if ((trit_number + CONSTANTS::UPPER) > _logical_size && new_trit != Trit::Unknown)
		{
			_logical_size = trit_number + CONSTANTS::UPPER;
			is_changed = true;
		}
		else if (new_trit == Trit::Unknown && (trit_number + CONSTANTS::UPPER) == _logical_size)
		{
			_logical_size = Length();
			is_changed = true;
		}

		if (is_changed || new_trit == Trit::True || new_trit == Trit::False)
			_unknown_trit_num = Length() - (_false_trit_num + _true_trit_num);
	}

	TritSet::Reference::operator Trit ()
	{
		return _trit_set.GetTrit(_trit_number);
	}

	TritSet& TritSet::Reference::operator = (Trit trit)
	{
		_trit_set.SetTrit(trit, _trit_number);
		return _trit_set;
	}

	size_t TritSet::Capacity() const
	{
		return _array_size;
	}

	void TritSet::Trim(size_t last_index)
	{
		size_t array_index = Background::GetArrayIndex(last_index);
		int bit_number = Background::GetBitIndex(last_index);

		_trits[array_index] &= ((CONSTANTS::FIRST_MASK << bit_number) << CONSTANTS::BIT_SHIFTER);

		for (size_t i = ++array_index; i < _array_size; ++i)
			_trits[i] = CONSTANTS::DEFAULT_VALUE;

		ChangeTritsNumber();
	}

	size_t TritSet::Cardinality(Trit value) const
	{
		if (value == Trit::True)
			return _true_trit_num;
		else if (value == Trit::False)
			return _false_trit_num;
		else
			return _unknown_trit_num;
	}

	void TritSet::Shrink()
	{
		size_t last_index = Length() - CONSTANTS::LOWERER;
		size_t array_index = Background::GetArrayIndex(last_index);
		if (array_index == CONSTANTS::ERROR)
		{
			_array_size = _logical_size = CONSTANTS::DEFAULT_VALUE;
			_trits.resize(_array_size);
		}
		else if ((array_index + CONSTANTS::UPPER) < _array_size)
		{
			_array_size = array_index + CONSTANTS::UPPER;
			_logical_size = last_index + CONSTANTS::UPPER;
			_trits.resize(_array_size);
		}
	}

	size_t TritSet::Size() const
	{
		return _logical_size;
	}

	size_t TritSet::GetArrayValue(size_t index) const
	{
		return _trits[index];
	}

	TritSet operator ~ (const TritSet& set)
	{
		TritSet new_trit_set(set.Size());

		for (size_t i = 0; i < set.Size(); ++i)
			new_trit_set[i] = ~set[i];

		return new_trit_set;
	}

	TritSet operator & (const TritSet& first_set, const TritSet& second_set)
	{
		size_t size = (first_set.Size() > second_set.Size()) ? first_set.Size() : second_set.Size();

		TritSet new_trit_set(size);

		for (size_t i = 0; i < size; ++i)
			new_trit_set[i] = first_set[i] & second_set[i];

		return new_trit_set;
	}

	TritSet operator | (const TritSet& first_set, const TritSet& second_set)
	{
		size_t size = (first_set.Size() > second_set.Size()) ? first_set.Size() : second_set.Size();

		TritSet new_trit_set(size);

		for (size_t i = 0; i < size; ++i)
			new_trit_set[i] = first_set[i] | second_set[i];

		return new_trit_set;
	}

	bool operator == (const TritSet& first_set, const TritSet& second_set)
	{
		if (first_set.Size() != second_set.Size())
			return false;

		for (size_t i = 0; i < first_set.Size(); ++i)
		{
			if (first_set[i] != second_set[i])
				return false;
		}

		return true;
	}

	bool operator != (const TritSet& first_set, const TritSet& second_set)
	{
		return !(first_set == second_set);
	}
}
