#include "background.h"
#include "constants.h"

namespace Background
{
	size_t GetArrayIndex(const size_t trit_number)
	{
		return (trit_number * CONSTANTS::TRIT_SIZE + CONSTANTS::SHIFT) / CONSTANTS::SIZE_T_IN_BITS - CONSTANTS::LOWERER;
	}

	int GetBitIndex(const size_t trit_number)
	{
		return (CONSTANTS::SIZE_T_IN_BITS - (((trit_number)*CONSTANTS::TRIT_SIZE) % CONSTANTS::SIZE_T_IN_BITS)) - 2 * CONSTANTS::LOWERER;
	}

	bool NotInRange(size_t array_index, const size_t array_size)
	{
		return ((++array_index) > array_size);
	}

	void SetValues(int& first_bit_value, int& second_bit_value, TernarySpace::Trit value)
	{
		if (value == TernarySpace::Trit::Unknown)
			return;

		if (value == TernarySpace::Trit::True)
		{
			first_bit_value = 0;
			second_bit_value = 1;
		}
		else if (value == TernarySpace::Trit::False)
		{
			first_bit_value = 1;
			second_bit_value = 0;
		}
	}

	void SetBit(size_t& value, int bit_number, int bit)
	{
		if (bit)
		{
			value |= ((size_t)1 << bit_number);
		}
		else
		{
			value &= ~((size_t)1 << bit_number);
		}
	}
}
