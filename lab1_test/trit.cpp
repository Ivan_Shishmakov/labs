#include "trit.h"

namespace TernarySpace
{
	Trit operator ~ (Trit trit)
	{
		if (trit == Trit::False)
			return Trit::True;
		else if (trit == Trit::True)
			return Trit::False;
		return Trit::Unknown;
	}

	Trit operator & (Trit first_trit, Trit second_trit)
	{
		if (first_trit == Trit::False || second_trit == Trit::False)
			return Trit::False;
		else if (first_trit == Trit::Unknown || second_trit == Trit::Unknown)
			return Trit::Unknown;
		return Trit::True;
	}

	Trit operator | (Trit first_trit, Trit second_trit)
	{
		if (first_trit == Trit::True || second_trit == Trit::True)
			return Trit::True;
		else if (first_trit == Trit::Unknown || second_trit == Trit::Unknown)
			return Trit::Unknown;
		return Trit::False;
	}
}