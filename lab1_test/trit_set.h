#ifndef TRIT_SPACE_H
#define TRIT_SPACE_H
#include "trit.h"
#include "constants.h"
#include <vector>

namespace TernarySpace
{
	class TritSet
	{
	public:
		TritSet(size_t = 0); 
		~TritSet(); 
		TritSet(const TritSet&);

		class Reference
		{
		public:
			Reference(TritSet&, size_t);
			~Reference();
			TritSet& operator = (Trit);
			operator Trit ();
		private:
			TritSet& _trit_set;
			size_t _trit_number;
		};

		size_t Capacity() const; 
		size_t Cardinality(Trit) const; 
		void Trim(size_t); 
		void Shrink(); 

		Reference operator [] (size_t); 
		Trit operator [] (size_t) const; 
		TritSet& operator = (const TritSet&); 
		size_t Size() const; 
		size_t Length() const; 

	private:
		void SetTrit(Trit, size_t);
		void ChangeArraySize(size_t);
		Trit GetTrit(size_t) const;
		size_t GetArrayValue(size_t) const;
		void ChangeTritsNumber(const Trit&, const Trit&);
		void ChangeTritsNumber();

		size_t _array_size = CONSTANTS::DEFAULT_VALUE;
		size_t _logical_size = CONSTANTS::DEFAULT_VALUE;
		size_t _true_trit_num = CONSTANTS::DEFAULT_VALUE;
		size_t _false_trit_num = CONSTANTS::DEFAULT_VALUE;
		size_t _unknown_trit_num = CONSTANTS::DEFAULT_VALUE;
		std::vector<size_t> _trits;
	};

	// BitWise Operators
	TritSet operator ~ (const TritSet&);
	TritSet operator & (const TritSet&, const TritSet&);
	TritSet operator | (const TritSet&, const TritSet&);


	// Additional operators for tests;
	bool operator == (const TritSet&, const TritSet&);
	bool operator != (const TritSet&, const TritSet&);
}

#endif
